from mash import util, config
from mash.commands import COMMANDS, PERMISSIONS, SCOPE, reply
from mash.database import db, DiscordUsers
from mash.modules import admin, gitissues, invite
import discord
import time
import re
from traceback import format_exc

import logging

mashu = discord.Client()
logger = logging.getLogger(__name__)
hook_channels = []
hook_loop = None


@mashu.event
async def on_ready():
    global hook_loop

    log_channel = mashu.get_channel(config.log_channel)
    discord_handler = util.DiscordHandler(log_channel, mashu)
    discord_handler.setLevel(logging.INFO)
    discord_handler.setFormatter(util.extended_formatter)
    logging.root.addHandler(discord_handler)

    for channel in config.webhook_channels:
        hook_channels.append(mashu.get_channel(channel))

    hook_server = util.PushEventServer(mashu, hook_channels)
    hook_loop = mashu.loop.create_task(hook_server.push_server())

    return True


@mashu.event
async def on_disconnect():
    global hook_loop

    if hook_loop:
        hook_loop.cancel()


@mashu.event
async def on_message(message):
    if message.author == mashu.user:
        if not message.guild:
            logger.info("Sent a message to %s (%s): %s" % (message.channel.recipient, message.channel.recipient.id,
                                                            message.content))
        elif message.channel.id != config.log_channel:
            logger.info("Sent a message to #%s (%s): %s" % (message.channel, message.channel.id,
                                                             message.content))
        return
    if message.author.bot:
        return

    if not message.guild or message.channel.id in config.allowed_channels:
        for thismatch, (module, funcname, permission, scope) in COMMANDS.items():
            if re.match(thismatch, message.content):
                if not message.guild:
                    logger.info("Received a message from %s (%s): %s" % (message.channel.recipient, message.channel.recipient.id,
                                                                          message.content))
                else:
                    logger.info("Received a message from #%s (%s): %s" % (message.channel, message.channel.id,
                                                                           message.content))

                user_permission = db.query(DiscordUsers.permissions).filter(DiscordUsers.discord_id == message.author.id).first()
                if not user_permission:
                    user_permission = PERMISSIONS.Default
                else:
                    user_permission = user_permission[0]
                if not user_permission & PERMISSIONS.Restricted:
                    if (user_permission | PERMISSIONS.Default) & permission:
                        if (message.guild and scope[0] & (SCOPE.Any | SCOPE.Channel)) or (
                                not message.guild and scope[0] & (SCOPE.Any | SCOPE.PM)):
                            try:
                                await getattr(module, funcname)((message, scope))
                            except Exception as e:
                                await reply((message, scope), "An unexpected error has occurred.")
                                logger.critical("Caught an exception with %s.%s:" % (module.__name__, funcname))
                                logger.critical(repr(e))
                                logger.critical(format_exc())
                    else:
                        await reply((message, scope), "You do not have the required permissions to use this command.")
                else:
                    await reply((message, scope), "You cannot use the bot. Please contact the bot owner.")


if __name__ == '__main__':
    util.init_logging()
    logger.debug("Logging started.")
    mashu.run(config.discord_client_secret)


