import re
import pathlib
import time
from ftplib import FTP
import ftplib
import base64
import urllib.request
import os
from argparse import ArgumentParser

from mash import ftpconfig

show_regex = re.compile(r'(^|(?P<group>\[.*?\])?[ _\.\-]?(?P<show>.*?)[ _\.\-]+)\#?(EP?[ _\.\-]?)?(?P<ep>\d{1,3})((-|-?EP?)(?P<ep2>\d{1,3})|)?[ _\.]?(V\d)?([ _\.\-]+(?P<title>.*))?$',
                        flags=re.IGNORECASE)
replacements = {'Shingeki no Kyojin (Season 3 Part 2)': 'Shingeki no Kyojin Season 3'}

parser = ArgumentParser()
parser.add_argument("-f", "--file", dest="filename",
                    help="Upload filename")

arguments = parser.parse_args()


def cdTree(ftp, currentDir):
    if currentDir != "":
        try:
            ftp.cwd(currentDir)
        except ftplib.all_errors as e:
            if "550 The system cannot find the file specified." in e.__str__():
                cdTree(ftp, "/".join(currentDir.split("/")[:-1]))
                ftp.mkd(currentDir)
                print("Created directory %s." % currentDir)
                ftp.cwd(currentDir)
            else:
                raise

def download_file(filename):
    print("Airing anime uploader script started.")


    # skip batches
    if not filename.endswith('.mkv'):
        print("Not a .mkv, exiting.")
        return None

    # check file exists
    if not pathlib.Path(filename).exists():
        print("File does not exist or cannot be accessed, exiting.")
        return None

    # we don't want
    if "horriblesubs" in filename.lower() and "1080p" not in filename.lower():
        print("Don't need HS 720p, exiting.")
        return None

    file = pathlib.Path(filename)
    print("Filename: %s" % file.name)
    print("File path: %s" % file)

    components = re.match(show_regex, file.name)
    show_name = components['show']
    for show, rpl in replacements.items():
        if show.lower() in file.name.lower():
            show_name = rpl  # replace with new folder name
    show_name = show_name.replace('.', ' ')

    show_name = re.sub(r'[^\x00-\x7F\x80-\xFF\u0100-\u017F\u0180-\u024F\u1E00-\u1EFF]', u' ', show_name)
    print("Folder name: %s" % show_name)
    print("-----------")
    maxtries = 5
    tries = 0
    while 1:
        tries += 1
        if tries > maxtries:
            print("Maximum retries encountered, aborting!")
            break
        try:
            ftp = FTP(ftpconfig.FTP_HOST, ftpconfig.FTP_USER, ftpconfig.FTP_PASSWORD)
            ftp.encoding = "utf-8"
            ftp.set_pasv(False)
            print("Logged into FTP.")
            break
        except Exception as e:
            print(repr(e))
            print("FTP gave an error, retrying.")
            time.sleep(2)

    cdTree(ftp, "/" + show_name)
    episode = open(file, 'rb')
    print("Uploading file %s." % os.path.join("/", show_name, re.sub(r'[^\x00-\x7F\x80-\xFF\u0100-\u017F\u0180-\u024F\u1E00-\u1EFF]', u' ', file.name)))
    print(ftp.storbinary('STOR %s' % re.sub(r'[^\x00-\x7F\x80-\xFF\u0100-\u017F\u0180-\u024F\u1E00-\u1EFF]', u' ', file.name), episode))
    print("Finished. Sending discord announcement...")
    truncated_file_name = file.name[:-4]
    title_to_announce = base64.urlsafe_b64encode(truncated_file_name.encode('utf-8'))
    urllib.request.urlopen("https://hooks.tearsinra.in/webhook/airing?key=%s&episode=%s" % (ftpconfig.airing_hook_key, title_to_announce.decode('utf-8')))
    print("-----------")
    print("\r\n")


if __name__ == "__main__":
    testfile = "/storage/rtorrent2/[Over-Time] Star☆Twinkle Precure - 13 [D3CC885C].mkv"
    testfile = "/storage/rtorrent2/[Over-Time] Star☆Twinkle Precure - 13 [D3CC885C].mkv"
    if 'filename' in arguments and arguments.filename:
        filename = arguments.filename
    else:
        filename = testfile
    download_file(filename)
