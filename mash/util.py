import logging
import logging.handlers
import base64
import logging.config
import discord
from enum import IntFlag
from mash import commands
from mash.database import db, Snippets
import asyncio
from aiohttp import web
from mash import config
import asyncio
from mash import webhooks
import re
from discord.ext.commands import clean_content
logger = logging.getLogger(__name__)

DEFAULT_LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
}

extended_formatter = logging.Formatter(
    "[%(asctime)s] [%(levelname)s] [%(name)s] [%(funcName)s():%(lineno)s] %(message)s",
    "%d/%m/%Y %H:%M:%S")


def prep(message):
    transformations = {
        re.escape(c): '\\' + c
        for c in ('*', '`', '_', '~', '\\', '|')
    }

    def replace(obj):
        return transformations.get(re.escape(obj.group(0)), '')

    pattern = re.compile('|'.join(transformations.keys()))
    message = pattern.sub(replace, message)
    return re.sub(r'@(everyone|here|[!&]?[0-9]{17,21})', '@\u200b\\1', message)


class DiscordHandler(logging.Handler):

    def __init__(self, channel, client):
        super().__init__()
        self._channel = channel
        self._client = client

    def emit(self, record):
        if self._client.is_ready():
            try:
                text = self.format(record)
                asyncio.create_task(self._channel.send(prep(text)))
            except Exception:
                try:
                    asyncio.create_task(self.handleError(record))
                except Exception:
                    raise


class PushEventServer:
    def __init__(self, bot, hook_channels):
        self.bot = bot
        self.hook_channels = hook_channels

    async def push_server(self):
        async def gitlab_handler(request):
            if request.method != 'POST':
                return web.json_response({'method': 'GET', 'status': 'failure', 'message': 'POST only permitted'}, status=405)

            if request.headers.get('X-Gitlab-Event') and request.headers.get('X-Gitlab-Token'):
                if request.headers.get('X-Gitlab-Token') == config.gitlab_webhook_token:
                    body = await request.json()
                    if 'event_name' in body:
                        key = 'event_name'
                    else:
                        key = 'event_type'
                    if body[key] == 'issue':
                        await webhooks.new_issue_opened(body,
                                    self.hook_channels)
                        return web.json_response({'method': 'POST', 'status': 'success'}, status=200)
                    elif body[key] == 'push':
                        await webhooks.file_change_git(body,
                                                       self.hook_channels)
                        return web.json_response({'method': 'POST', 'status': 'success'}, status=200)
                    else:
                        return web.json_response({'method': 'POST', 'status': 'failure', 'message': 'Bad request'}, status=400)
            else:
                return web.json_response({'method': 'POST', 'status': 'failure', 'message': 'Bad request'}, status=400)

        async def airing_handler(request):
            body_key = request.rel_url.query['key']
            body_episode = request.rel_url.query['episode']
            if body_key != config.airing_hook_key:
                return web.json_response({'method': 'POST', 'status': 'failure', 'message': 'Bad request'}, status=400)
            else:
                if body_episode:
                    embed_text = base64.b64decode(body_episode).decode('utf-8')
                    embed = discord.Embed(title="New airing anime", description=embed_text, color=0x7E29D2)
                    for channel in self.hook_channels:
                        await channel.send(embed=embed)
                    return web.json_response({'method': 'POST', 'status': 'success'}, status=200)
                else:
                    return web.json_response({'method': 'POST', 'status': 'failure', 'message': 'Bad request'}, status=400)

        async def snippet_handler(request):
            snippet_number = request.match_info['snippetno']
            try:
                snippet_val = int(snippet_number)
            except ValueError:
                return web.Response(text="405 Malformed Request", status=400)
            if snippet_val <= 0:
                return web.Response(text="405 Malformed Request", status=400)
            else:
                snippet_db = db.query(
                    Snippets.snippet).filter(
                    Snippets.id == snippet_val
                ).first()

                if not snippet_db:
                    return web.Response(text="404 Not Found", status=404)
                else:
                    return web.Response(text="%s" % snippet_db.snippet, status=200)

        app = web.Application()
        app.router.add_routes([
            web.route('*', '/webhook/gitlab', gitlab_handler),
            web.get('/snippets/{snippetno}', snippet_handler),
            web.route('*', '/webhook/airing', airing_handler)])
        runner = web.AppRunner(app)
        await runner.setup()

        self.site = web.TCPSite(runner, '0.0.0.0', config.CLIENT_HOOK_PORT)
        await self.bot.wait_until_ready()
        await self.site.start()

    def __unload(self):
        asyncio.ensure_future(self.site.stop())


def init_logging():
    logging.config.dictConfig(DEFAULT_LOGGING)
    # [PID:%(process)d TID:%(thread)d]
    # default_formatter = logging.Formatter("[%(asctime)s] [%(levelname)s] %(message)s",
    # "%d/%m/%Y %H:%M:%S")

    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.INFO)
    console_handler.setFormatter(extended_formatter)

    file_handler = logging.handlers.RotatingFileHandler('../logs/mashu.log', maxBytes=1024 * 1024 * 10, backupCount=300, encoding='utf-8')
    file_handler.setFormatter(extended_formatter)
    file_handler.setLevel(logging.DEBUG)

    logging.root.setLevel(logging.INFO)
    logging.root.addHandler(console_handler)
    logging.root.addHandler(file_handler)
