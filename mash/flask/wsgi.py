from mash.flask.webhooks import create_app
import os
from mash import config

if __name__ == '__main__':
    app = create_app(os.path.abspath(config.__file__))  # starts the flask server
    app.run(host=app.config.get("HOOK_HOST", "0.0.0.0"),
            port=app.config.get("HOOK_PORT", 5000),
            debug=app.config.get("DEBUG", False), )
