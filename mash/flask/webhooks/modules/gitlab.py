import json
from flask import Blueprint, request
import sys
import mash.commands
import flask
import mash.util
import asyncio
from flask import current_app as app

bp = Blueprint('gitlab', __name__)


@bp.route('/webhook/gitlab', methods=['GET', 'POST'])
def handle_gitlab_webhook():
    if not request.method == 'POST':
        return flask.jsonify({'method': 'GET', 'status': 'failure', 'message': 'POST only permitted'}), 405

    if request.headers.get('X-Gitlab-Event') and request.headers.get('X-Gitlab-Token'):
        if request.headers.get('X-Gitlab-Token') == mash.config.gitlab_webhook_token:
            if request.headers.get('X-Gitlab-Event') == 'Issue Hook':
                body = request.data
                asyncio.create_task(mash.commands.new_issue_opened(body,
                                    app.config['hook_channels']))
                return flask.jsonify({'method': 'POST', 'status': 'success'}), 200
    else:
        return flask.jsonify({'method': 'POST', 'status': 'failure', 'message': 'Bad request'}), 400
    return flask.jsonify("pong")
