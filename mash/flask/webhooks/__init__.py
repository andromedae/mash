import flask
from flask import abort
import os

from werkzeug import find_modules, import_string


def create_app(config: str, hook_channels: object) -> flask.Flask:
    app = flask.Flask(__name__,
                      instance_relative_config=True,
                      static_folder='./static',
                      template_folder='./templates'
                      )
    app.config.from_pyfile(config)
    app.config['hook_channels'] = hook_channels

    with app.app_context():
        register_blueprints(app)
        register_error_handlers(app)

    return app


def register_blueprints(app: flask.Flask) -> None:
    for name in find_modules('mash', recursive=True):
        mod = import_string(name)
        if hasattr(mod, 'bp'):
            app.register_blueprint(mod.bp)


def register_error_handlers(app: flask.Flask) -> None:
    app.register_error_handler(404, lambda _: (flask.render_template("404.html", page_title="404 Not Found"), 404))


if __name__ == '__main__':
    app = create_app('/home/py/mash/mash/config.py')
    app.run(host=app.config.get("HOOK_HOST", "0.0.0.0"),
            port=app.config.get("HOOK_PORT", 5000),
            debug=app.config.get("DEBUG", False))