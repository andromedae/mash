import importlib
import sys
from mash.commands import register_command, COMMANDS, reply, PERMISSIONS, SCOPE


@register_command(
    '^!reload$',
    permission=(
            PERMISSIONS.Administrator
    ),
    scope=(
            SCOPE.Any,
            SCOPE.Any
    )
)
async def reload(message):
    for thismatch, (module, funcname, permission, scope) in COMMANDS.items():
        if module.__name__ != '__main__':
            COMMANDS[thismatch] = (
                importlib.reload(sys.modules[module.__name__]),
                funcname,
                permission,
                scope
            )
    await reply(message, "Reloaded modules.")


@register_command(
    '^!url$',
    permission=(
            PERMISSIONS.Member |
            PERMISSIONS.Administrator
    ),
    scope=(
            SCOPE.Any,
            SCOPE.PM
    )
)
async def url_get(message):
    await reply(message, "https://plex.andromedae.moe/")

@register_command(
    '^!quit$',
    permission=(
            PERMISSIONS.Administrator
    ),
    scope=(
            SCOPE.Any,
            SCOPE.Any
    )
)
async def exit_bot(_):
    sys.exit(0)
