from mash.commands import register_command, reply, SCOPE, PERMISSIONS
import re
from mash.config import gitlab_token, issues_project, issues_assignee
from mash.database import db, DiscordUsers, PlexUsers, PlexIssues
import logging
import datetime
from sqlalchemy import and_
import asyncio
from plexapi.server import PlexServer
import plexapi.exceptions
import discord
import gitlab

logger = logging.getLogger(__name__)

gl = gitlab.Gitlab('https://gitlab.com', private_token=gitlab_token)
project = gl.projects.get(issues_project)


@register_command(
    '^!issue .*$',
    permission=(
            PERMISSIONS.Member |
            PERMISSIONS.Moderator |
            PERMISSIONS.Administrator
    ),
    scope=(
            SCOPE.Any,
            SCOPE.Any
    )
)
async def command_new_issue(m):
    await handle_new_issue(m, 0)


@register_command(
    '^!request .*$',
    permission=(
            PERMISSIONS.Member |
            PERMISSIONS.Moderator |
            PERMISSIONS.Administrator
    ),
    scope=(
            SCOPE.Any,
            SCOPE.Any
    )
)
async def command_new_request(m):
    await handle_new_issue(m, 1)


async def handle_new_issue(m, issue_type):
    message, _ = m
    snippet = message.content.split(' ', 1)[1].replace('\n', ' ')
    if len(snippet) > 28:
        snippet = snippet[0:25] + "..."
    if issue_type:
        label = 'New content'
        title = '%s requested new content: %s' % (str(message.author), snippet)
    else:
        label = 'Existing content'
        title = 'Issue from %s: %s' % (str(message.author), snippet)

    # check they aren't spamming
    open_issues = db.query(PlexIssues.id).join(DiscordUsers, PlexIssues.discord_user == DiscordUsers.id).filter(
        and_(DiscordUsers.discord_id == message.author.id,
             PlexIssues.closed == False)  # noqa

    ).count()
    if open_issues >= 5:
        await reply(m, "You already have 5 issues open. Please edit your previous issues.")
        return

    user_id = db.query(DiscordUsers.id).filter(DiscordUsers.discord_id == message.author.id).first()[0]  # this should always return
    issue = project.issues.create({'title': title,
                                   'description': message.content.split(' ', 1)[1] + "\r\n\r\n" + "<@" + str(message.author.id) + ">",
                                   'labels': [label],
                                   'assignee_ids': [issues_assignee]})

    if issue:
        new_issue = PlexIssues(
            discord_user=user_id,
            issue_id=issue.attributes['iid']
        )
        db.add(new_issue)
        db.commit()
    await reply(m, "Issue %s opened." % issue.attributes['iid'])


@register_command(
    '^!editissue .*$',
    permission=(
            PERMISSIONS.Member |
            PERMISSIONS.Moderator |
            PERMISSIONS.Administrator
    ),
    scope=(
            SCOPE.Any,
            SCOPE.Any
    )
)
async def edit_issue(m):
    message, _ = m
    split_message = message.content.split(' ', 2)
    if not split_message[2]:
        await reply(m, "Incomplete command.")

    issue_id = split_message[1]
    issue_text = split_message[2]

    issue_db = db.query(
        DiscordUsers.discord_id,
        DiscordUsers.permissions
    ).join(PlexIssues, PlexIssues.discord_user == DiscordUsers.id).filter(
        and_(
            PlexIssues.issue_id == issue_id,
            PlexIssues.closed == False
        )
    ).first()

    if not issue_db:
        await reply(m, "Cannot edit this issue because it is closed or does not exist.")
        return
    if issue_db[0] == message.author.id or get_perms(message.author.id) & (PERMISSIONS.Moderator | PERMISSIONS.Administrator):
        editable_issue = project.issues.get(issue_id)
        old_text = editable_issue.attributes['description']
        edit_text = "\r\n\r\nEdited by %s at %s:\r\n" % (str(message.author), datetime.datetime.utcnow())
        editable_issue.description = old_text + edit_text + issue_text
        editable_issue.save()
        logger.info("Successfully edited an issue %s." % issue_id)
        await reply(m, "Edited issue %s." % issue_id)
    else:
        await reply(m, "You do not own this issue and thus cannot edit it.")
        return


@register_command(
    '^!listissues$',
    permission=(
            PERMISSIONS.Member |
            PERMISSIONS.Moderator |
            PERMISSIONS.Administrator
    ),
    scope=(
            SCOPE.Any,
            SCOPE.Any
    )
)
async def list_issues(m):
    message, _ = m

    issue_list = db.query(
        PlexIssues.issue_id,
    ).join(DiscordUsers, PlexIssues.discord_user == DiscordUsers.id).filter(
        and_(
            DiscordUsers.discord_id == message.author.id,
            PlexIssues.closed == False
        )
    )

    if not issue_list:
        await reply(m, "You currently do not have any open issues.")
        return

    issues_open = []
    for issue in issue_list:
        issues_open.append(str(issue[0]))

    if not issues_open:
        await reply(m, "You currently do not have any open issues.")
        return

    await reply(m, "You have the following issue(s) open: %s" % (', '.join(issues_open)))

@register_command(
    '^!openissues$',
    permission=(
            PERMISSIONS.Moderator |
            PERMISSIONS.Administrator
    ),
    scope=(
            SCOPE.Any,
            SCOPE.Any
    )
)
async def open_issues(m):
    message, _ = m

    issue_list = db.query(
        PlexIssues.issue_id,
    ).filter(
            PlexIssues.closed == False
    )

    if not issue_list:
        await reply(m, "There are no open issues.")
        return

    issues_open = []
    for issue in issue_list:
        issues_open.append(str(issue[0]))

    if not issues_open:
        await reply(m, "There are no open issues.")
        return

    await reply(m, "The following issue(s) are open: %s" % (', '.join(issues_open)))


@register_command(
    '^!getissue \d+$',
    permission=(
            PERMISSIONS.Member |
            PERMISSIONS.Moderator |
            PERMISSIONS.Administrator
    ),
    scope=(
            SCOPE.Any,
            SCOPE.Any
    )
)
async def get_issue(m):
    message, _ = m

    split_message = message.content.split(' ', 1)
    issue_id = split_message[1]

    issue_db = db.query(
        DiscordUsers.discord_id,
        DiscordUsers.permissions,
        PlexIssues.closed
    ).join(PlexIssues, PlexIssues.discord_user == DiscordUsers.id).filter(
        PlexIssues.issue_id == issue_id
    ).first()

    if not issue_db:
        await reply(m, "This issue does not exist.")
        return
    if issue_db[0] == message.author.id or get_perms(message.author.id) & (PERMISSIONS.Moderator | PERMISSIONS.Administrator):
        issue_contents = project.issues.get(issue_id)
        if issue_db[2]:
            await reply(m, "Displaying closed issue %s:" % issue_id)
        else:
            await reply(m, "Displaying open issue %s:" % issue_id)

        await reply(m, "%s" % issue_contents.attributes['title'])
        await reply(m, "```%s```" % issue_contents.attributes['description'])
    else:
        await reply(m, "You do not own this issue and thus cannot view it.")
        return


@register_command(
    '^!closeissue \d+$',
    permission=(
            PERMISSIONS.Member |
            PERMISSIONS.Moderator |
            PERMISSIONS.Administrator
    ),
    scope=(
            SCOPE.Any,
            SCOPE.Any
    )
)
async def close_issue(m):
    message, _ = m

    split_message = message.content.split(' ', 1)
    issue_id = split_message[1]

    issue_db = db.query(
        DiscordUsers,
        PlexIssues
    ).join(PlexIssues, PlexIssues.discord_user == DiscordUsers.id).filter(
        and_(
            PlexIssues.issue_id == issue_id,
            PlexIssues.closed == False
        )
    ).first()

    if not issue_db:
        await reply(m, "This issue does not exist or is already closed.")
        return
    if (
            issue_db[0].discord_id == message.author.id
    ) or (
            get_perms(message.author.id) & (PERMISSIONS.Moderator | PERMISSIONS.Administrator)
    ):
        issue_to_close = project.issues.get(issue_id)
        issue_to_close.state_event = 'close'
        issue_to_close.save()

        issue_db[1].closed = True
        db.commit()
        await reply(m, "Closed issue %s." % issue_id)
    else:
        await reply(m, "You do not own this issue and thus cannot close it.")
        return


@register_command(
    '^!openissue \d+$',
    permission=(
            PERMISSIONS.Moderator |
            PERMISSIONS.Administrator
    ),
    scope=(
            SCOPE.Any,
            SCOPE.Any
    )
)
async def open_issue(m):
    message, _ = m

    split_message = message.content.split(' ', 1)
    issue_id = split_message[1]

    issue_db = db.query(
        DiscordUsers,
        PlexIssues
    ).join(PlexIssues, PlexIssues.discord_user == DiscordUsers.id).filter(
        and_(
            PlexIssues.issue_id == issue_id,
            PlexIssues.closed == True
        )
    ).first()

    if not issue_db:
        await reply(m, "This issue does not exist or is already open.")
        return
    if (
            issue_db[0].discord_id == message.author.id
    ) or (
            get_perms(message.author.id) & (PERMISSIONS.Moderator | PERMISSIONS.Administrator)
    ):
        issue_to_open = project.issues.get(issue_id)
        issue_to_open.state_event = 'reopen'
        issue_to_open.save()

        issue_db[1].closed = False
        db.commit()
        await reply(m, "Opened issue %s." % issue_id)
    else:
        await reply(m, "You do not own this issue and thus cannot open it.")
        return


def get_perms(id):
    return db.query(
        DiscordUsers.permissions,
    ).filter(
            DiscordUsers.discord_id == id
    ).first()[0]
