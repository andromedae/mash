from mash.commands import register_command, reply, SCOPE, PERMISSIONS
import re
from mash.config import plex_address, plex_token, server_id, libraries, blacklisted_email_match
from mash.database import db, DiscordUsers, PlexUsers
import logging
import asyncio
from plexapi.server import PlexServer
import plexapi.exceptions
import discord
from traceback import format_exc

logger = logging.getLogger(__name__)

email_regex = re.compile(r"([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)", re.IGNORECASE)
pending_invite_emails = {}
pending_updates = {}


def search_user(discord_id):
    return db.query(PlexUsers.email).join(
        DiscordUsers, PlexUsers.discord_user == DiscordUsers.id
    ).filter(
        DiscordUsers.discord_id == discord_id
    ).first()


@register_command(
    '^!invite .*$',
    scope=(
            SCOPE.Any,
            SCOPE.PM
    )
)
async def handle_invite(m):
    message, _ = m

    # check we are in a pm, if not, delete message and tell user
    if message.guild:
        try:
            await message.delete()
            await reply(m, "Your message (`%s`) was deleted from #%s to protect your privacy. Please resend it "
                           "here, in a private message." % (message.content, message.channel))
        except discord.errors.Forbidden:
            logger.warning("Unable to delete a message, this bot needs permissions.")
            await reply(m, "Please resend your message (`%s`) here, in a private message." % message.content)
        return

    captured_email = re.match(email_regex, message.content.split(' ', 1)[1])
    if captured_email:
        found_email = search_user(message.author.id)
        for blacklisted in blacklisted_email_match:
            if blacklisted in captured_email[0]:
                await reply(m, "You cannot use a university email address to sign up to this service. Please pick a different one.")
                return

        if found_email and found_email[0] != captured_email[0]:
            pending_updates[message.author.id] = (captured_email[0], found_email[0])
            await reply(m, "This discord account already has the email %s registered to it. Updating your email "
                           "address will remove access to the old email.\r\n"
                           "Update your email address? (**Yes**/**No**)" % found_email[0])
        elif found_email:
            await reply(m, "You have already registered. https://plex.andromedae.moe/")
        else:
            pending_invite_emails[message.author.id] = captured_email[0]
            await reply(m, "Please type YES if you agree to the following terms and conditions of the Andromeda Plex "
                           "Server, or NO to stop the invitation process:")
            await reply(m, "```1. The Plex server is not officially associated with the society or SUSU. As such, its existence should not be "
                           "advertised outside of the society discord and in person. Do not post about the Plex server on Facebook or other websites."
                           "\r\n2. If your account remains unused for an extended period of time, "
                           "it may be removed to make space for other users. If this happens, please re-register.\r\n"
                           "3. Please use \"direct play\" where possible to reduce the load on the server.```"
                        )
            await reply(m, "Have you read and do you agree to the above? (**Yes**/**No**):")
    else:
        await reply(m, "No valid email address found. Please reissue the command with a valid email address.")


@register_command(
    "^(yes|no|y|n)$",
    scope=(
            SCOPE.PM,
            SCOPE.PM
    )
)
async def handle_invite_part2(m):
    message, _ = m
    contents = message.content.lower()
    if 'yes' in contents or 'y' in contents:
        if message.author.id in pending_updates or message.author.id in pending_invite_emails:
            plex = PlexServer(plex_address, plex_token).myPlexAccount()
            if message.author.id in pending_updates:
                old_email = pending_updates[message.author.id][1]
                db.query(PlexUsers).filter(PlexUsers.email == old_email).delete()
                db.commit()
                try:
                    plex.removeFriend(old_email)
                    logger.info("Removed %s from Plex." % old_email)
                except plexapi.exceptions.NotFound:
                    logger.critical("Could not remove %s from Plex. User already deleted?" % old_email)  # continue anyway

            # handle new invite
            if message.author.id in pending_invite_emails:
                new_email = pending_invite_emails[message.author.id]
            else:
                new_email = pending_updates[message.author.id][0]

            try:
                plex.inviteFriend(user=new_email,
                                  server=server_id,
                                  sections=libraries)
                logger.info("Added %s to Plex." % new_email)

            except Exception as e:
                logger.critical("Error occurred while inviting %s:" % new_email)
                logger.critical(repr(e))
                logger.critical(format_exc())
                await reply(m, "An error has occurred inviting this email address. Please contact the bot owner.")
                pending_updates.pop(message.author.id, None)
                pending_invite_emails.pop(message.author.id, None)
                return

            # this isn't the best way but I'm new to sql ORM
            userid = db.query(DiscordUsers.id).filter(DiscordUsers.discord_id == message.author.id).first()
            if not userid:
                new_user = DiscordUsers(discord_id=message.author.id, permissions=int(PERMISSIONS.Default | PERMISSIONS.Member),
                                        name=str(message.author))
                db.add(new_user)
                db.flush()
                new_user_plex = PlexUsers(email=new_email, discord_user=new_user.id)
                db.add(new_user_plex)
                db.flush()
                db.commit()
            else:
                new_user_plex = PlexUsers(email=new_email, discord_user=userid[0])
                db.add(new_user_plex)
                db.flush()
                db.commit()

            await reply(m, "You have been invited to the Plex server. Please accept the invite sent to your email address and create an account if you do not have one. If you "
                           "mistyped your email, please start again with !invite new@ema.il.")
            await reply(m, "Afterwards, visit https://plex.andromedae.moe/ to use the Plex server in your browser, or download the Plex app to your computer, smart TV or phone.")
            await reply(m, "**You will need to click the link in your email before you can see anything.**")
            await reply(m, "The android app requires a small payment (or purchase of a plex pass) to be usable. If you want to avoid this, download and install the modified apk "
                           "here instead of using the app store and disable auto-updates: https://violet.andromedae.moe/apps/Plex-v7.12.2.9471_build_717377521.apk")
            await reply(m, "Enjoy! (*・ω・)ﾉ")
    else:
        if message.author.id in pending_updates or message.author.id in pending_invite_emails:
            await reply(m, "Invitation process terminated. Type !invite my@ema.il to restart the process.")

    pending_updates.pop(message.author.id, None)
    pending_invite_emails.pop(message.author.id, None)
