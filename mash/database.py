from sqlalchemy import Column, Integer, String, Float, DateTime, func, BigInteger, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker
from mash.commands import PERMISSIONS
from mash.config import sql
from sqlalchemy import create_engine, ForeignKey

engine = create_engine(sql)
session = sessionmaker()
session.configure(bind=engine)
db = session()
Base = declarative_base()


class DiscordUsers(Base):
    __tablename__ = 'discord_users'

    id = Column('id', Integer, primary_key=True, autoincrement=True)
    discord_id = Column('discord_id', BigInteger, unique=True)
    permissions = Column('permissions', Integer)
    date = Column('date', DateTime(timezone=True), default=func.now())
    name = Column('name', String, default="")


class PlexUsers(Base):
    __tablename__ = 'plex_users'

    id = Column('id', Integer, primary_key=True, autoincrement=True)
    email = Column('email', String)
    date = Column('date', DateTime(timezone=True), default=func.now())
    discord_user = Column('discord_user', Integer, ForeignKey(DiscordUsers.id), unique=True)


class PlexIssues(Base):
    __tablename__ = 'plex_issues'

    id = Column('id', Integer, primary_key=True, autoincrement=True)
    date = Column('date', DateTime(timezone=True), default=func.now())
    discord_user = Column('discord_user', Integer, ForeignKey(DiscordUsers.id))
    issue_id = Column('issue_id', Integer)
    closed = Column('closed', Boolean, default=False)


class Snippets(Base):
    __tablename__ = 'snippets'

    id = Column('id', Integer, primary_key=True, autoincrement=True)
    snippet = Column('snippet', String)


Base.metadata.create_all(engine, checkfirst=True)
