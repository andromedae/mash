import re
import sys
from functools import wraps
import logging
import discord
from enum import IntFlag

COMMANDS = {}

logger = logging.getLogger(__name__)


class PERMISSIONS(IntFlag):
    Default = 2**1
    Restricted = 2**2
    Member = 2**3
    Moderator = 2**4
    Administrator = 2**5


class SCOPE(IntFlag):
    Any = 2**1
    Channel = 2**2
    PM = 2**3


def register_command(command, permission=PERMISSIONS.Default, scope=(SCOPE.Any, SCOPE.Any)):
    """
    Register command for the bot.
    :param command: Regex to be recognized.
    :param permission: Permission mask required.
    :param scope: Scope tuple (command, reply)
    :return:
    """
    def wrapper(func):
        key = re.compile(command, flags=re.IGNORECASE)
        COMMANDS[key] = (sys.modules[func.__module__], func.__name__, permission, scope)

        @wraps(func)
        async def new_func(*args, **kwargs):
            await func(*args, **kwargs)

        return new_func

    return wrapper


async def reply(mtuple, string):
    message, (scope_recv, scope_reply) = mtuple
    try:
        if not message.guild and scope_reply & (SCOPE.Any | SCOPE.PM):
            await message.author.send(string)
        elif message.guild and scope_reply & (SCOPE.Any | SCOPE.Channel):
            await message.channel.send(string)
        elif message.guild and scope_reply & SCOPE.PM:
            await message.author.send(string)
        else:
            logger.critical("An illegal message reply was attempted (%s, %s)." % (scope_reply, string))
    except discord.errors.Forbidden:
        logger.critical("Could not send a message, either blocked or kicked from server. (%s, %s)" % (scope_reply,
                                                                                                      string))

