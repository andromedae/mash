import discord
from mash.database import db, Snippets

async def new_issue_opened(issue_object, hook_channels):
    title = issue_object['object_attributes']['title']
    description = issue_object['object_attributes']['description']
    action = issue_object['object_attributes']['action']

    if 'requested new content' in title:
        color = 0x00FF1E
    else:
        color = 0x20f4ff

    if action == 'close':
        action_text = 'Closed'
    elif action == 'open':
        action_text = 'Opened'
    elif action == 'update':
        action_text = 'Updated'
    elif action == 'reopen':
        action_text = 'Reopened'
    else:
        action_text = '???'

    embed = discord.Embed(title=("Issue %s" % action_text), color=color)
    embed.add_field(name=title, value=description, inline=False)
    for channel in hook_channels:
        await channel.send(embed=embed)


async def file_change_git(file_object, hook_channels):
    for commit in file_object['commits']:
        commit_files_added = []
        commit_files_moved = []
        commit_files_removed = []
        commit_files_added = commit['added']
        commit_files_moved = commit['modified']
        commit_files_removed = commit['removed']

        cfcount = 0
        cfs = []
        cfmore = 0
        cf_full = []
        for file in commit_files_added:
            cf_full.append("+ " + file)
            if cfcount > 9:
                cfmore += 1
                cfcount += 1
            else:
                cfs.append("+ " + file)
                cfcount += 1

        for file in commit_files_moved:
            cf_full.append("% " + file)
            if cfcount > 9:
                cfmore += 1
                cfcount += 1
            else:
                cfs.append("% " + file)
                cfcount += 1

        for file in commit_files_removed:
            cf_full.append("- " + file)
            if cfcount > 9:
                cfmore += 1
                cfcount += 1
            else:
                cfs.append("- " + file)
                cfcount += 1

        if cf_full and cfmore > 0:
            new_snippet = Snippets(
                snippet="\r\n".join(cf_full)
            )
            db.add(new_snippet)
            db.commit()

        if cfs and cf_full:
            embed_text = "\r\n".join(cfs)
            if cfmore > 0:
                embed_text = embed_text + ("\r\n\r\n...and [%i more](https://violet.andromedae.moe/snippets/%i)" % (cfmore, new_snippet.id))
            embed = discord.Embed(title="Content update", description=embed_text, color=0xa600a6)
            for channel in hook_channels:
                await channel.send(embed=embed)
